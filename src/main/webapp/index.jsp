<%-- 
    Document   : index.jsp
    Created on : 11-06-2019, 04:40:52 PM
    Author     : Elena
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <title>Read</title>
    </head>
    <body>
        <p><strong>Spring con JSP</strong></p>
        <table border="1">
            <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Direccion</th>
                <th>Action</th>
            </tr>
            <c:forEach var="cli" items="${lsClis}">
                <tr>
                    <td>${cli.nombre}</td>
                    <td>${cli.apellido}</td>
                    <td>${cli.direccion}</td>
                    <td><a href="update/<c:out value='${cli.id}'/>">Update</a> | <a
                            href="delete/<c:out value='${cli.id}'/>">Delete</a></td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
