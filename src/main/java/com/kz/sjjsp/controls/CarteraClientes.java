package com.kz.sjjsp.controls;

import com.kz.sjjsp.models.Cliente;
import com.kz.sjjsp.utils.Dao;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Kz
 */
@Controller
public class CarteraClientes {

    @Autowired
    private Dao clients;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ModelAndView createStudent(@RequestParam("nombre") String nom, @RequestParam("apellido") String ape,
            @RequestParam("direccion") String dir, ModelAndView mv) {

        Cliente cli = new Cliente();
        cli.setNombre(nom);
        cli.setApellido(ape);
        cli.setDireccion(dir);

        clients.create(cli);

        mv.setViewName("index");

        return mv;
    }

    @RequestMapping(value = "/read")
    public ModelAndView readStudent(ModelAndView model) throws IOException {

        List<Cliente> listStudent = clients.read();
        model.addObject("lsClis", listStudent);
        model.setViewName("index");

        return model;
    }
}
