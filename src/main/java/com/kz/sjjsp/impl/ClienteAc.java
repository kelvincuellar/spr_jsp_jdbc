package com.kz.sjjsp.impl;

import com.kz.sjjsp.models.Cliente;
import com.kz.sjjsp.utils.Dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Kz
 */
@Repository
public class ClienteAc implements Dao<Cliente> {

    @Autowired
    Connection dataSource;
    private PreparedStatement p;
    private String[] ar = {
        "insert into cliente(nombre,apellido,direccion, fecha_nacimiento, telefono,email) values(?,?,?,?,?,?)",
        "select * from cliente order by id_cliente",
        "select * from cliente order by id_cliente where id_cliente=?",
        "update cliente set nombre=?, apellido=?, direccion=?, fecha_nacimiento=?, telefono=?, email=? where id_cliente=?",
        "delete from cliente where id_cliente=?"};

    private Cliente e;
    private List<Cliente> c;

    public ClienteAc() {
    }

    @Override
    public void create(Cliente t) {
        try {
            p = dataSource.prepareStatement(ar[0]);
            p.setString(1, t.getNombre());
            p.setString(2, t.getApellido());
            p.setString(3, t.getDireccion());
            p.setString(4, String.valueOf(t.getFechaNac()));
            p.setString(5, t.getTelefono());
            p.setString(6, t.getEmail());
            p.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error en Cliente: Al crear");
        } finally {
            System.out.println("Recuerde cerrar la conexion");
        }
    }

    @Override
    public List<Cliente> read() {
        try {
            // p = Enlace.conecta().prepareStatement(ar[0]);
            p = dataSource.prepareStatement(ar[1]);
            ResultSet rs = p.executeQuery();
            c = new ArrayList<Cliente>();
            while (rs.next()) {
                e = new Cliente();
                e.setId(rs.getLong("id_cliente"));
                e.setNombre(rs.getString("nombre"));
                e.setApellido(rs.getString("apellido"));
                e.setDireccion(rs.getString("direccion"));
                e.setFechaNac(rs.getDate("fecha_nacimiento"));
                e.setTelefono(rs.getString("telefono"));
                e.setEmail(rs.getString("email"));
                c.add(e);
            }
            return c;
        } catch (Exception e) {
            System.out.println("Error en Cliente: Al leer");
            return null;
        } finally {
            System.out.println("Recuerde cerrar la conexion");
        }
    }

    @Override
    public Cliente readBy(Object id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Cliente t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Cliente t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
